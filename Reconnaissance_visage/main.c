#include <opencv/opencv.h>
#include <opencv/highgui.h>
#include <stdio.h>

int main(void){
    CvHaarClassifierCascade *cascade;       // Déclaration du classificateur
    CvMemStorage *storage;                  // Déclaration de l'espace de stockage pour le traitement
    CvCapture *capture;                     // Déclaration de la capture de la webcam
    IplImage *frame;                        // Déclaration de l'image source
    
    cascade = (CvHaarClassifierCascade*)cvLoad("haarcascade_frontalface_alt.xml", 0, 0, 0 );    // Chargement du classificateur
    capture = cvCreateCameraCapture(CV_CAP_ANY);                                                // Initialisation de la webcam
    storage = cvCreateMemStorage(0);                                                            // Allocation de l'espace de stockage pour le traitement
    cvNamedWindow("Entree", 1 );                                                                // Creation des fenetres
    cvNamedWindow("Sortie", 1 );
    
    /*        !!!          ------- TRAITEMENT -------        !!!         */
    
    while(cvWaitKey(10)!='!'){                                                                                                  // Tant que la touche '!' n'est pas pressee
        img = cvQueryFrame(capture);                                                                                            // Recupere la dernière image de la webcaùm
        cvShowImage("Entree", img);                                                                                             // Affiche l'image recuperee
        
        CvSeq *faces = cvHaarDetectObjects(img, cascade, storage, 1.1, 3 , 0, cvSize(40, 40));                                  /*** !!! Detection des visages selon le classificateur !!! ***/
        
        for (i = 0 ; i < (faces ? faces->total : 0) ; i++){                                                                     // Parcours du tableau contenant les visages trouves
            CvRect *r = (CvRect*)cvGetSeqElem(faces, i);                                                                        // Création du rectangle entourant le visage
            cvRectangle(img, cvPoint(r->x, r->y), cvPoint(r->x + r->width, r->y + r->height), CV_RGB(255, 0, 0), 1, 8, 0);      // Intégration du rectangle dans l'image de sortie
        }
        
        cvShowImage("Sortie", img)                                                                                              // Affichage de l'image de sortie
    }
    
    /*        !!!          ------- FIN TRAITEMENT -------        !!!         */
    
    cvReleaseCapture(&capture);                     // Liberation de la capture de la webcam
    cvDestroyWindow("Entree");                      // Destruction des fenetres
    cvDestroyWindow("Sortie");
    cvReleaseHaarClassifierCascade(&cascade);       // Liberation du classificateur
    cvReleaseMemStorage(&storage);                  // Liberation de l'espace de stockage pour le traitement
}